drop table member;
create table member (
	mid varchar(20) primary key,
	name varchar(20) not null,
	password varchar(20) not null
);

drop table board;
create table board (
	bno number(5) primary key,
	title varchar(100) not null,
	content varchar(4000) not null,
	writer varchar(20) not null,
	writeday date not null,
	hitcount number(3) not null
);

drop sequence board_bno;
create sequence board_bno;

insert into member values ('winter', '최하얀', '12345');
insert into member values ('spring', '박나리', '54321');
insert into board values (
	board_bno.nextval, 
	'함박눈이 내려요', 
	'함박눈이 오면 좋은 소식이 옵니다.', 
	'winter',
	sysdate,
	3);
insert into board values (
	board_bno.nextval, 
	'봄은 언제 올까요', 
	'봄이 얼렁 왔으면 좋겠어요', 
	'spring',
	sysdate,
	5);
commit;

