<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title></title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>Spring Form Tag</h1>
				<a href="#" data-rel="back" data-icon="arrow-l">이전</a>
			</div>	
								
			<div data-role="content">
				<form:form method="post" action="getFormData" commandName="member">
					<div data-role="fieldcontain">
						<form:label path="mid">아이디</form:label>
						<form:input path="mid"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="mname">이름</form:label>
						<form:input path="mname"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="mpassword">패스워드</form:label>
						<form:password path="mpassword"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="mage">나이</form:label>
						<form:input path="mage"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="mbirth">가입일</form:label>
						<form:input path="mbirth"/>
					</div>
										
					<form:hidden path="mnickname"/>
										
					<div data-role="fieldcontain">
						<form:label path="mjob">직업</form:label>
						<form:select path="mjob" data-native-menu="false">
							<form:option value="">--선택하세요--</form:option>
							<form:options items="${jobList}"/>
						</form:select>
					</div>
					
					<div data-role="fieldcontain">
					    <fieldset data-role="controlgroup">
							<legend>취미</legend>
							<form:checkbox path="mhobbys" label="여행" value="여행"/>
							<form:checkbox path="mhobbys" label="스포츠" value="스포츠"/>
							<form:checkbox path="mhobbys" label="영화" value="영화"/>
					    </fieldset>
					</div>

					<div data-role="fieldcontain">
					    <fieldset data-role="controlgroup" data-type="horizontal">
							<legend>성별</legend>
							<form:radiobutton path="msex" label="남자" value="남자"/>
							<form:radiobutton path="msex" label="여자" value="여자"/>
					    </fieldset>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="mcomment">하고싶은말</form:label>
						<form:textarea path="mcomment"/>
					</div>
					
					<input type="submit" data-theme="b" value="가입"/>
				</form:form>
			</div>
		</div>
	</body>
</html>