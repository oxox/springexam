<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title></title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>Spring Form Tag</h1>
				<a href="#" data-rel="back" data-icon="arrow-l">이전</a>
			</div>	
								
			<div data-role="content">
				아이디: ${member.mid} <br/>
				이름: ${member.mname} <br/>
				패스워드: ${member.mpassword} <br/>
				나이: ${member.mage} <br/>
				생일: ${member.mbirth} <br/>
				닉네임: ${member.mnickname} <br/>
				직업:
					<c:forEach var="mhobby" items="${member.mhobbys}" varStatus="status">
						${mhobby}
						<c:if test="${status.last==false}">,</c:if>
					</c:forEach> <br/>
				성별: ${member.msex} <br/>
				하고 싶은말: ${member.mcomment}
				
			</div>
		</div>
	</body>
</html>