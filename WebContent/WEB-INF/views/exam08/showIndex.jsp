<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title></title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>JDBC</h1>
				<a href="#" data-rel="back" data-icon="arrow-l">이전</a>
			</div>	
								
			<div data-role="content">
				<a href="boardList" data-role="button">게시목록</a>
				<a href="#" data-role="button">회원목록</a>
			</div>
		</div>
	</body>
</html>