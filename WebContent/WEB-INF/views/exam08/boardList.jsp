<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title></title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>JDBC</h1>
				<a href="#" data-rel="back" data-icon="arrow-l">이전</a>
			</div>	
								
			<div data-role="content">
				<ul data-role="listview">
					<c:forEach var="board" items="${boards}">
						<li>
							<a href="boardDetailView?bno=${board.bno}">
							<table>
								<tr>
									<td style="width:50px">${board.bno}</td>
									<td style="width:50px">${board.title}</td>
								</tr>
							</table>
							</a>
						</li>
					</c:forEach>
				</ul>
			</div>
			
			<div data-role="footer" data-position="fixed">
				<div data-role="navbar">
					<ul>
						<li><a href="boardInsertForm">쓰기</a></li>
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>