<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title></title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>JDBC</h1>
				<a href="#" data-rel="back" data-icon="arrow-l">이전</a>
			</div>	
								
			<div data-role="content">
				<form:form method="post" action="boardInsert" commandName="board">
					<div data-role="fieldcontain">
						<form:label path="title">제목</form:label>
						<form:input path="title" readonly="true"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="content">내용</form:label>
						<form:textarea path="content" readonly="true"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="writer">글쓴이</form:label>
						<form:input path="writer" readonly="true"/>
					</div>		
					
					<div data-role="fieldcontain">
						<form:label path="writeday">글쓴날짜</form:label>
						<form:input path="writeday" readonly="true"/>
					</div>
					
					<div data-role="fieldcontain">
						<form:label path="hitcount">조회수</form:label>
						<form:input path="hitcount" readonly="true"/>
					</div>
				</form:form>			
			</div>
			
			<div data-role="footer" data-position="fixed">
				<div data-role="navbar">
					<ul>
						<li><a href="boardDelete?bno=${board.bno}">삭제</a></li>
						<li><a href="boardUpdateForm?bno=${board.bno}">수정</a></li>
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>