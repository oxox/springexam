<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title>Home</title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>SpringExam</h1>
			</div>
			
			<div data-role="content">
				<ul data-role="listview">
					<li><a href="/springexam">Exam01:컨트롤러 기본</a></li>
					<li><a href="/springexam/exam02/sendParameter">Exam02:파라메터 매핑</a></li>
					<li><a href="/springexam/exam03/sendData">Exam03:문자 인코딩 필터</a></li>					
					<li><a href="/springexam/exam04/joinForm">Exam04:폼 데이터 받기</a></li>
					<li><a href="/springexam/exam05/showServletApi">Exam05:Servlet API 사용</a></li>
					<li><a href="/springexam/exam06/viewData">Exam06:View Data</a></li>
					<li><a href="/springexam/exam07/showForm">Exam07:Spring Form Tag</a></li>
					<li><a href="/springexam/exam08/showIndex">Exam08:JDBC</a></li>
					<li><a href="/springexam/exam09/downloadApk" data-ajax="false">Exam09:파일 다운로드</a></li>
				</ul>	
			</div>
		</div>
	</body>
</html>

