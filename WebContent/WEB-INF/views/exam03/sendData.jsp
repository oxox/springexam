<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title>Home</title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>Encoding</h1>
			</div>
			
			<div data-role="content">
				<h3>[POST 방식]</h3>
				<form method="post" action="getPostData">
					<input type="text" name="data" value="POST:스프링"/>
					<input type="submit" value="전송"/>
				</form>
				
				<h3>[GET 방식]</h3>
				<a href="javascript:sendGetData();" data-role="button">전송</a>
				<script type="text/javascript">
					function sendGetData() {
						var data = encodeURI("GET:스프링");
						console.log(data);
						location.href = "getGetData?data="+data;
					}
				</script>
			</div>
		</div>
	</body>
</html>
