<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title>Home</title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>View Data</h1>
				<a href="/springexam" data-icon="arrow-l">홈</a>
			</div>			
			<div data-role="content">
				<form method="post" action="join">
					<div data-role="fieldcontain">
						<label for="mid">아이디</label>
						<input id="mid" name="mid" type="text" value="${member.mid}"/>
					</div>
					<div data-role="fieldcontain">
						<label for="mname">이름</label>
						<input id="mname" name="mname" type="text" value="${member.mname}"/>
					</div>
					<div data-role="fieldcontain">
						<label for="mpassword">패스워드</label>
						<input id="mpassword" name="mpassword" type="password" value="${member.mpassword}"/>
					</div>
					<div data-role="fieldcontain">
						<label for="mage">나이</label>
						<input id="mage" name="mage" type="number" value="${member.mage}"/>
					</div>
					<div data-role="fieldcontain">
						<label for="mbirth">가입일</label>
						<input id="mbirth" name="mbirth" type="date" value="${member.mbirth}"/>
					</div>
					<input type="submit" value="가입"/>
				</form>
			</div>
		</div>
	</body>
</html>