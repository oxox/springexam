<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title>Home</title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1>View Data</h1>
				<a href="/springexam" data-icon="arrow-l">홈</a>
			</div>	
								
			<div data-role="content">
				이름: ${name}
				<hr/>
				번호: ${board.bno} <br/>
				제목: ${board.title} <br/>
				내용: ${board.content} <br/>
				글쓴이: ${board.writer} <br/>
				날짜: ${board.writeday} <br/>
				조회수: ${board.hitcount} <br/>
			</div>
		</div>
	</body>
</html>