<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title>Home</title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">	
			<div data-role="header">
				<h1>Parameter Mapping</h1>
			</div>		
			<div data-role="content">
				<a data-role="button" href="getParameter1?mid=mid1&bno=1">getParameter1</a>
				<a data-role="button" href="getParameter2?memberid=mid2&boardno=2">getParameter2</a>
				<a data-role="button" href="getParameter3?mid=mid3">getParameter3</a>
			</div>
		</div>
	</body>
</html>