<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
		<title>Home</title>
		<link rel="stylesheet" href="/springexam/resources/jqm/jquery.mobile-1.2.0.min.css" />
		<script src="/springexam/resources/jqm/jquery-1.8.2.min.js"></script>
		<script src="/springexam/resources/jqm/jquery.mobile-1.2.0.min.js"></script>		
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<h1></h1>
				<a href="/springexam" data-icon="arrow-l">홈</a>
			</div>			
			<div data-role="content">
				<ul data-role="listview">
					<li><a href="/springexam/exam05/useRequest">HttpServletRequest 사용</a></li>
					<li><a href="/springexam/exam05/useResponse">HttpServletResponse 사용</a></li>
					<li><a href="/springexam/exam05/useSession">HttpSession 사용</a></li>
				</ul>				
			</div>
		</div>
	</body>
</html>