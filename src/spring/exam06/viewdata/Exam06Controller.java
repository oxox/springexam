package spring.exam06.viewdata;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Exam06Controller {
	
	@RequestMapping("/exam06/viewData")
	public String viewData() {
		return "exam06/viewData";
	}
	
	@RequestMapping("/exam06/getData1")
	public String getData1(Model model) {
		model.addAttribute("name", "홍길동");
		
		Board board = new Board();
		board.setBno(1);
		board.setTitle("게시물 제목");
		board.setContent("게시물 내용");
		board.setWriter("hong");
		board.setWriteday(new Date(new java.util.Date().getTime()));
		board.setHitcount(5);
		
		model.addAttribute("board", board);
		
		return "exam06/getData1";
	}
	
	@RequestMapping("/exam06/showForm")
	public String showForm() {
		return "exam06/joinForm";
	}
	@RequestMapping("/exam06/getData2")
	public String getData2(Member member) {
		return "exam06/getData2";
	}
	
	@ModelAttribute("options")
	public List<String> getOptionData() {
		List<String> options = new ArrayList<String>();
		options.add("전체");
		options.add("아이템");
		options.add("캐릭터");
		return options;
	}
	@RequestMapping("/exam06/getData3")
	public String getData3() {
		return "exam06/getData3";
	}
}
