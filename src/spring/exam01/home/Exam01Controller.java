package spring.exam01.home;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Exam01Controller {
	
    @RequestMapping(value = "/")
    public String home(HttpServletRequest request) {
        System.out.println("HomeController: Passing through...");
        return "exam01/home";
    }
   
}

