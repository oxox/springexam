package spring.exam08.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class BoardDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public RowMapper<Board> rowMapper = new RowMapper<Board>() {
		@Override
		public Board mapRow(ResultSet rs, int rowNum) throws SQLException {
			Board board = new Board();
			board.setBno(rs.getInt("bno"));
			board.setTitle(rs.getString("title"));
			board.setContent(rs.getString("content"));
			board.setWriter(rs.getString("writer"));
			board.setWriteday(rs.getDate("writeday"));
			board.setHitcount(rs.getInt("hitcount"));
			return board;
		}
	};

	public List<Board> selectAll() {
		List<Board> boards = new ArrayList<Board>();
		boards = jdbcTemplate.query("select * from board", rowMapper);
		return boards;
	}
	
	public int insert(final Board board) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int rows = jdbcTemplate.update(
			new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
					PreparedStatement pstmt = conn.prepareStatement(
						"insert into board(bno,title,content,writer,writeday,hitcount) values(board_bno.nextval,?,?,?,sysdate,?)",
						new String[] {"bno"}
					);
					pstmt.setString(1, board.getTitle());
					pstmt.setString(2, board.getContent());
					pstmt.setString(3, board.getWriter());
					pstmt.setInt(4, board.getHitcount());
					return pstmt;
				}
			},
			keyHolder
		);
		if(rows != 0) {
			Number number =  keyHolder.getKey();
			return number.intValue();
		} else {
			return 0;
		}
	}
	
	public Board selectByBno(int bno) {
		Board board = jdbcTemplate.queryForObject("select * from board where bno=?", new Object[] {bno}, rowMapper);
		return board;
	}
	
	public int delete(int bno) {
		int rows = jdbcTemplate.update("delete from board where bno=?", bno);
		return rows;
	}
	
	public int update(Board board) {
		int rows = jdbcTemplate.update(
			"update board set title=?, content=? where bno=?", 
			board.getTitle(),
			board.getContent(),
			board.getBno()
		);
		return rows;
	}
}
