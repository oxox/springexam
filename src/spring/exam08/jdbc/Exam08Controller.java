package spring.exam08.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Exam08Controller {
	@Autowired
	private BoardDao boardDao;
	
	@ModelAttribute("board")
	public Board formBacking() {
		return new Board();
	}
	
	@RequestMapping("/exam08/showIndex")
	public String showIndex() {
		return "exam08/showIndex";
	}
	
	@RequestMapping("/exam08/boardList")
	public String boardList(Model model) {
		List<Board> boards = boardDao.selectAll();
		model.addAttribute("boards", boards);
		return "exam08/boardList";
	}

	@RequestMapping("/exam08/boardInsertForm")
	public String boardInsertForm() {
		return "exam08/boardInsertForm";
	}
	
	@RequestMapping("/exam08/boardInsert")
	public String boardInsert(Board board) {
		int bno = boardDao.insert(board);
		return "redirect:/exam08/boardList";
	}
	
	@RequestMapping("/exam08/boardDetailView")
	public String boardDetailView(@RequestParam int bno, Model model) {
		Board board = boardDao.selectByBno(bno);
		model.addAttribute("board", board);
		return "exam08/boardDetailView";
	}
	
	@RequestMapping("/exam08/boardDelete")
	public String boardDelete(@RequestParam int bno) {
		boardDao.delete(bno);
		return "redirect:/exam08/boardList";
	}
	
	@RequestMapping("/exam08/boardUpdateForm")
	public String boardUpdateForm(@RequestParam int bno, Model model) {
		Board board = boardDao.selectByBno(bno);
		model.addAttribute("board", board);
		return "exam08/boardUpdateForm";
	}
	
	@RequestMapping("/exam08/boardUpdate")
	public String boardUpdate(Board board) {
		int rows = boardDao.update(board);
		return "redirect:/exam08/boardList";
	}
}
