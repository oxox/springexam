package spring.exam07.formtag;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Exam07Controller {
	
	@ModelAttribute("jobList")
	public List<String> getJobList() {
		List<String> jobList = new ArrayList<String>();
		jobList.add("학생");
		jobList.add("디자이너");
		jobList.add("개발자");
		return jobList;
	}

	@ModelAttribute("member")
	public Member formBacking(HttpServletRequest request) {
		Member member = null;
		if(request.getMethod().equalsIgnoreCase("GET")) {
			member = new Member();
			member.setMid("white");
			member.setMname("최하얀");
			member.setMpassword("12345");
			member.setMage(25);
			member.setMbirth(new Date(new java.util.Date().getTime()));
			member.setMnickname("눈송이");
			member.setMjob("개발자");
			member.setMhobbys(new String[] {"여행", "영화"});
			member.setMsex("여자");
			member.setMcomment("30자 이내로 입력하세요");
		} else {
			member = new Member();
		}
		return member;
	}
	
	@RequestMapping("/exam07/showForm")
	public String showForm() {
		return "exam07/showForm";
	}
	
	@RequestMapping("/exam07/getFormData")
	public String getFormData(Member member) {
		return "exam07/getFormData";
	}
	
}
