package spring.exam07.formtag;

import java.sql.Date;

public class Member {
	private String mid;
	private String mname;
	private String mpassword;
	private int mage;
	private Date mbirth;
	private String mnickname;
	private String mjob;
	private String[] mhobbys;
	private String msex;
	private String mcomment;
	
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getMpassword() {
		return mpassword;
	}
	public void setMpassword(String mpassword) {
		this.mpassword = mpassword;
	}
	public int getMage() {
		return mage;
	}
	public void setMage(int mage) {
		this.mage = mage;
	}
	public Date getMbirth() {
		return mbirth;
	}
	public void setMbirth(Date mbirth) {
		this.mbirth = mbirth;
	}
	public String getMnickname() {
		return mnickname;
	}
	public void setMnickname(String mnickname) {
		this.mnickname = mnickname;
	}
	public String getMjob() {
		return mjob;
	}
	public void setMjob(String mjob) {
		this.mjob = mjob;
	}
	public String[] getMhobbys() {
		return mhobbys;
	}
	public void setMhobbys(String[] mhobbys) {
		this.mhobbys = mhobbys;
	}
	public String getMsex() {
		return msex;
	}
	public void setMsex(String msex) {
		this.msex = msex;
	}
	public String getMcomment() {
		return mcomment;
	}
	public void setMcomment(String mcomment) {
		this.mcomment = mcomment;
	}
	
}
