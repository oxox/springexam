package spring.exam05.servletapi;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Exam05Controller {
	
	@RequestMapping("/exam05/showServletApi")
	public String showServletApi() {
		return "exam05/servletApi";
	}
	
	@RequestMapping("/exam05/useRequest")
	public String useRequest(HttpServletRequest request) {
		String remoteAddr = request.getRemoteAddr();
		System.out.println(remoteAddr);
		return "redirect:/exam05/showServletApi";
	}
	
	@RequestMapping("/exam05/useResponse")
	public String useResponse(HttpServletResponse response) {
		try {
			response.sendRedirect("/springexam/exam05/showServletApi");
		} catch (IOException e) {}
		return null;
	}
	
	@RequestMapping("/exam05/useSession")
	public String useSession(HttpSession session) {
		Integer count = (Integer) session.getAttribute("count");
		if(count == null) {
			count = new Integer(0);
			session.setAttribute("count", count);
		}
		int value = count;
		value++;
		System.out.println("count:" + value);
		
		count = new Integer(value);
		session.setAttribute("count", count);
		return "redirect:/exam05/showServletApi";
	}
	
}
