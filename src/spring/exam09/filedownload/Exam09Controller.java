package spring.exam09.filedownload;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
public class Exam09Controller implements ApplicationContextAware {
	private WebApplicationContext context = null;

	@RequestMapping("/exam09/downloadApk")
	public String downloadApk(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String filePath = context.getServletContext().getRealPath("/WEB-INF/apk/jetboy.apk");
		File file = new File(filePath);
		response.setContentType("application/vnd.android.package-archive");
		response.setContentLength((int) file.length());

		String userAgent = request.getHeader("User-Agent");
		boolean ie = userAgent.indexOf("MSIE") > -1;
		String fileName = null;
		if (ie) {
			fileName = URLEncoder.encode(file.getName(), "utf-8");
		} else {
			fileName = new String(file.getName().getBytes("utf-8"), "iso-8859-1");
		}
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		OutputStream out = response.getOutputStream();
		FileInputStream fis = new FileInputStream(file);
		
		FileCopyUtils.copy(fis, out);
		
		fis.close();
		out.flush();
		return null;
	}
	
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = (WebApplicationContext) applicationContext;
	}
}
