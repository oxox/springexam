package spring.exam03.encoding;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/*
[web.xml]에 추가할 내용
---------------------
<filter>
    <filter-name>encodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
	<init-param>
	    <param-name>encoding</param-name>
	    <param-value>euc-kr</param-value>
	</init-param>
</filter>
<filter-mapping>
    <filter-name>encodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
----------------------
*/

@Controller
public class Exam03Controller {
	
	@RequestMapping("/exam03/sendData")
	public String sendForm() {
		return "exam03/sendData";
	}
	
	@RequestMapping(value="/exam03/getPostData", method=RequestMethod.POST)
	public String getPostData(@RequestParam String data) {
		System.out.println(data);
		return "redirect:/exam03/sendData";
	}
	
	@RequestMapping(value="/exam03/getGetData", method=RequestMethod.GET)
	public String getGetData(@RequestParam String data) {
		try {
			data = new String(data.getBytes("8859_1"), "utf-8");
		} catch(Exception e) {}
		System.out.println(data);
		return "redirect:/exam03/sendData";
	}	
	
}
