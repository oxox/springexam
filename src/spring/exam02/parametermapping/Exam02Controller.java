package spring.exam02.parametermapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Exam02Controller {
	
	@RequestMapping("/exam02/sendParameter")
	public String sendPage() {
		return "exam02/sendParameter";
	}

	@RequestMapping("/exam02/getParameter1")
	public String getParameter1(
			@RequestParam String mid, 
			@RequestParam int bno) {
		System.out.println(mid + ":" + bno);
		return "redirect:/exam02/sendParameter";
	}
	
	@RequestMapping("/exam02/getParameter2")
	public String getParameter2(
			@RequestParam("memberid") String mid, 
			@RequestParam("boardno") int bno) {
		System.out.println(mid + ":" + bno);
		return "redirect:/exam02/sendParameter";
	}
	
	@RequestMapping("/exam02/getParameter3")
	public String getParameter3(
			@RequestParam String mid, 
			@RequestParam(value="bno", required=false, defaultValue="3") int bno) {
		System.out.println(mid + ":" + bno);
		return "redirect:/exam02/sendParameter";
	}
	
}
