package spring.exam04.formcommand;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Exam04Controller {

	@RequestMapping(value="/exam04/joinForm", method=RequestMethod.GET)
	public String joinForm() {
		return "exam04/joinForm";
	}
	
	@RequestMapping(value="/exam04/join", method=RequestMethod.POST)
	public String join(Member member) {
		System.out.println("아이디: " + member.getMid());
		System.out.println("이름: " + member.getMname());
		System.out.println("비밀번호: " + member.getMpassword());
		System.out.println("나이: " + member.getMage());
		System.out.println("가입일: " + member.getMbirth());
		return "redirect:/exam04/joinForm";
	}
}
